package linkedlist

import (
	"testing"
	"fmt"
)

func TestSetDoubleLinkedListHeadWithHeadNil(t *testing.T) {
	node1 := &Node{1, nil, nil}
	ll := NewDoublyLinkedList()
	ll.SetHead(node1)
	if ll.Head.Value != 1 {
		t.FailNow()
	}
}

func TestSetDoubleLinkedListHeadWithHead(t *testing.T) {
	node1 := &Node{1, nil, nil}
	node2 := &Node{2, nil, nil}
	ll := NewDoublyLinkedList()
	ll.SetHead(node1)
	if ll.Head.Value != 1 {
		t.FailNow()
	}

	if ll.Tail.Value != 1 {
		t.FailNow()
	}

	ll.SetHead(node2)

	if ll.Head.Value != 2 {
		t.FailNow()
	}

	if ll.Head.Next.Value != 1 {
		t.FailNow()
	}

	if ll.Head.Next.Next != nil {
		t.FailNow()
	}

	if ll.Tail.Value != 1 {
		t.FailNow()
	}
}

func TestSetTailOnDoubleLinkedListWithNilTail(t *testing.T) {
	node := &Node{1, nil, nil}
	ll := NewDoublyLinkedList()
	ll.SetTail(node)

	if ll.Head.Value != 1 || ll.Tail.Value != 1 {
		t.FailNow()
	}

	node1 := &Node{2, nil, nil}
	ll.SetTail(node1)

	if ll.Tail.Value != 2 {
		t.FailNow()
	}

	if ll.Tail.Prev.Value != 1 {
		t.FailNow()
	}
}

func TestRemoveNodeFromDoubleLinkedListWithOneNode(t *testing.T) {
	node := &Node{1, nil, nil}
	ll := NewDoublyLinkedList()
	ll.SetHead(node)
	ll.Remove(node)

	if ll.Head != nil && ll.Tail != nil {
		t.FailNow()
	}
}

func TestRemovingHeadNode(t *testing.T) {
	node := &Node{1, nil, nil}
	ll := NewDoublyLinkedList()
	ll.SetHead(node)
	node2 := &Node{2, nil, nil}
	ll.SetTail(node2)
	if ll.Head.Value != 1 && ll.Tail.Value != 2 {
		t.FailNow()
	}
	ll.Remove(node)

	if ll.Head.Value != 2 && ll.Tail.Value != 2 {
		t.FailNow()
	}

	if ll.Head != ll.Tail {
		t.FailNow()
	}
}

func TestRemovingTailNode(t *testing.T) {
	node := &Node{1, nil, nil}
	ll := NewDoublyLinkedList()
	ll.SetHead(node)
	node2 := &Node{2, nil, nil}
	ll.SetTail(node2)
	if ll.Head.Value != 1 && ll.Tail.Value != 2 {
		t.FailNow()
	}
	ll.Remove(node2)

	if ll.Head.Value != 1 && ll.Tail.Value != 1 {
		t.FailNow()
	}

	if ll.Head != ll.Tail {
		t.FailNow()
	}
}

func TestRemovingNode(t *testing.T) {
	node := &Node{1, nil, nil}
	ll := NewDoublyLinkedList()
	ll.SetHead(node)
	node2 := &Node{2, nil, nil}
	ll.SetTail(node2)
	node3 := &Node{3,nil,nil}
	ll.SetTail(node3)
	if ll.Head.Value != 1 && ll.Head.Next.Value != 2{
		t.FailNow()
	}

	if ll.Tail.Value != 3 && ll.Tail.Prev.Value != 2{
		t.FailNow()
	}

	ll.Remove(node2)

	if ll.Head.Value != 1 && ll.Tail.Value != 3{
		t.FailNow()
	}

	if ll.Head.Next != ll.Tail{
		t.FailNow()
	}

}

func TestRemovingNodeWithValue(t *testing.T) {
	node := &Node{1, nil, nil}
	ll := NewDoublyLinkedList()
	ll.SetHead(node)
	node2 := &Node{2, nil, nil}
	ll.SetTail(node2)
	node3 := &Node{3,nil,nil}
	ll.SetTail(node3)
	if ll.Head.Value != 1 && ll.Head.Next.Value != 2{
		t.FailNow()
	}

	if ll.Tail.Value != 3 && ll.Tail.Prev.Value != 2{
		t.FailNow()
	}

	ll.RemoveNodesWithValue(2)

	if ll.Head.Value != 1 && ll.Tail.Value != 3{
		t.FailNow()
	}

	if ll.Head.Next != ll.Tail{
		t.FailNow()
	}

}

func TestRemovingNodeWithNonExistingValue(t *testing.T) {
	node := &Node{1, nil, nil}
	ll := NewDoublyLinkedList()
	ll.SetHead(node)
	node2 := &Node{2, nil, nil}
	ll.SetTail(node2)
	node3 := &Node{3,nil,nil}
	ll.SetTail(node3)
	if ll.Head.Value != 1 && ll.Head.Next.Value != 2{
		t.FailNow()
	}

	if ll.Tail.Value != 3 && ll.Tail.Prev.Value != 2{
		t.FailNow()
	}

	ll.RemoveNodesWithValue(5)

	if ll.Head.Value != 1 && ll.Tail.Value != 3{
		t.FailNow()
	}

	if ll.Head.Next == ll.Tail{
		t.FailNow()
	}

}

func TestFindValueInList(t *testing.T){
	node := &Node{1, nil, nil}
	ll := NewDoublyLinkedList()
	ll.SetHead(node)
	node2 := &Node{2, nil, nil}
	ll.SetTail(node2)
	node3 := &Node{3,nil,nil}
	ll.SetTail(node3)

	if !ll.ContainsNodeWithValue(3){
		t.FailNow()
	}

	if ll.ContainsNodeWithValue(5) {
		t.FailNow()
	}
}

func TestInsertBeforeHeadNode(t *testing.T){
	node := &Node{1,nil,nil}
	ll := NewDoublyLinkedList()
	ll.SetHead(node)
	node2 := &Node{2,nil,nil}
	ll.InsertBefore(node,node2)

	if ll.Head != node2 {
		t.FailNow()
	}
}

func TestInsertBeforeTailNode(t *testing.T){
	node := &Node{1,nil,nil}
	ll := NewDoublyLinkedList()
	ll.SetHead(node)
	node2 := &Node{3,nil,nil}
	ll.SetTail(node2)
	nodeToInsert := &Node{3,nil,nil}
	ll.InsertBefore(node2,nodeToInsert)

	if ll.Head.Next != nodeToInsert {
		t.FailNow()
	}

	if ll.Tail.Prev != nodeToInsert {
		t.FailNow() 
	}
}

func TestDoubleLinkedList1(t *testing.T){
	node1 := &Node{1,nil,nil}
	node2 := &Node{2,nil,nil}
	node3 := &Node{3,nil,nil}
	node3_2 := &Node{3,nil,nil}
	node3_3 := &Node{3,nil,nil}
	node4 := &Node{4,nil,nil}
	node5 := &Node{5,nil,nil}
	node6 := &Node{6,nil,nil}

	ll := NewDoublyLinkedList()

	ll.SetHead(node5)
	ll.PrintList()
	fmt.Println("*****************************")
	ll.SetHead(node4)
	ll.PrintList()
	fmt.Println("*****************************")
	ll.SetHead(node3)
	ll.PrintList()
	fmt.Println("*****************************")
	ll.SetHead(node2)
	ll.PrintList()
	fmt.Println("*****************************")
	ll.SetHead(node1)
	ll.PrintList()
	fmt.Println("*****************************")
	ll.SetHead(node4)
	ll.PrintList()
	fmt.Println("*****************************")
	ll.SetTail(node6)
	ll.PrintList()
	fmt.Println("*****************************")
	ll.InsertBefore(node6,node3)
	ll.PrintList()
	fmt.Println("*****************************")
	ll.InsertAfter(node6,node3_2)
	ll.PrintList()
	fmt.Println("*****************************")
	ll.InsertAtPosition(1,node3_3)
	ll.PrintList()
	fmt.Println("*****************************")
	ll.RemoveNodesWithValue(3)
	ll.PrintList()
	fmt.Println("*****************************")
	ll.Remove(node2)
	ll.PrintList()
	fmt.Println("*****************************")
	ll.ContainsNodeWithValue(5)
	ll.PrintList()
	fmt.Println("*****************************")

	//ll.PrintList()



}

func TestInsertAtPosition(t *testing.T){
	node := &Node{1,nil,nil}
	ll := NewDoublyLinkedList()
	ll.InsertAtPosition(1,node)
}

