package linkedlist

import "fmt"

type Node struct {
	Value      int
	Prev, Next *Node
}

type DoublyLinkedList struct {
	Head, Tail *Node
}

func NewDoublyLinkedList() *DoublyLinkedList {
	return &DoublyLinkedList{}
}

func (ll *DoublyLinkedList) SetHead(node *Node) {
	head := ll.Head
	if head == nil {
		ll.Head = node
		ll.Tail = node
	} else {
		ll.InsertBefore(ll.Head,node)
	}
}

func (ll *DoublyLinkedList) SetTail(node *Node) {

	tail := ll.Tail
	if tail == nil {
		ll.Head = node
		ll.Tail = node
	} else {
		ll.InsertAfter(ll.Tail,node)
	}
}

func (ll *DoublyLinkedList) InsertBefore(node, nodeToInsert *Node) {
	// Write your code here.
	if ll.ContainsNode(nodeToInsert){
		ll.Remove(nodeToInsert)
	}
	prev := node.Prev

	if prev == nil { // inserting before the head

		tempHead := ll.Head
		ll.Head = nodeToInsert
		nodeToInsert.Next = tempHead
		tempHead.Prev = nodeToInsert

	} else {
		nodeToInsert.Next = node
		nodeToInsert.Prev = prev
		prev.Next = nodeToInsert
		node.Prev = nodeToInsert
	}

}

func (ll *DoublyLinkedList) InsertAfter(node, nodeToInsert *Node) {
	// Write your code here.
	if ll.ContainsNode(nodeToInsert){
		ll.Remove(nodeToInsert)
	}
	next := node.Next
	if next == nil { // inserting after tailnode
		temp := ll.Tail
		node.Next = nodeToInsert
		nodeToInsert.Prev = temp
		ll.Tail = nodeToInsert
	} else {
		ll.InsertBefore(next, nodeToInsert)
	}
}

func (ll *DoublyLinkedList) InsertAtPosition(position int, nodeToInsert *Node) {
	// Write your code here.
	
	head := ll.Head

	if head == nil || position == 1 {
		ll.SetHead(nodeToInsert)
		return
	}
	for head != nil && position != 1 {
		head = head.Next
		position--
	}
	if head != nil {
		ll.InsertBefore(head, nodeToInsert)
	}else {
		ll.SetTail(nodeToInsert)
	}

}

func (ll *DoublyLinkedList) RemoveNodesWithValue(value int) {
	// Write your code here.
	head := ll.Head
	for head != nil {
		tempHead := head
		head = head.Next
		if tempHead.Value == value {
			ll.Remove(tempHead)
		}
	}

}

func (ll *DoublyLinkedList) Remove(node *Node) {

	if node == nil {
		return
	}

	prev := node.Prev
	next := node.Next

	//if there is only one element
	if prev == nil && next == nil {
		ll.Head = nil
		ll.Tail = nil

	} else if prev == nil { // if removing head
		next.Prev = nil
		ll.Head = next
	} else if next == nil { // if removing tail
		prev.Next = nil
		ll.Tail = prev
	} else { // if removing non-head and non-tail Node
		prev.Next = next
		next.Prev = prev
	}
	node.Next = nil
	node.Prev = nil
}

func (ll *DoublyLinkedList) ContainsNodeWithValue(value int) bool {
	// Write your code here.
	head := ll.Head
	for head != nil {
		if head.Value == value {
			return true
		}
		head = head.Next
	}
	return false
}

func (ll *DoublyLinkedList) ContainsNode(node *Node) bool {
	// Write your code here.
	head := ll.Head
	for head != nil {
		if head == node {
			return true
		}
		head = head.Next
	}
	return false
}

func (ll *DoublyLinkedList) PrintList(){

	head := ll.Head

	for head != nil {
		fmt.Printf("Double Value:%d",head.Value)
		fmt.Println()
		head = head.Next
	}
}
