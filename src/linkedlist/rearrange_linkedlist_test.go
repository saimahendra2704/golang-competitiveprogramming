package linkedlist

import "testing"

func TestRearrangeLinkedList1(t *testing.T) {
	list := getLinkedList()
	list.setHead(&node{1, nil, false})
	list.add(&node{0, nil, false})
	list.add(&node{2, nil, false})
	list.add(&node{5, nil, false})
	list.add(&node{3, nil, false})
	list.add(&node{4, nil, false})
	rearrangeLinkedList(list, 3)

	if list.head.value != 1 {
		t.FailNow()
	}

	if list.head.next.value != 0 {
		t.FailNow()
	}

	if list.head.next.next.value != 2 {
		t.FailNow()
	}

	if list.head.next.next.next.value != 3 {
		t.FailNow()
	}

	if list.head.next.next.next.next.value != 5 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.value != 4 {
		t.FailNow()
	}

}

func TestRearrangeLinkedList2(t *testing.T) {
	list := getLinkedList()
	list.setHead(&node{3, nil, false})
	list.add(&node{0, nil, false})
	list.add(&node{2, nil, false})
	list.add(&node{5, nil, false})
	list.add(&node{1, nil, false})
	list.add(&node{4, nil, false})
	rearrangeLinkedList(list, 3)

	if list.head.value != 0 {
		t.FailNow()
	}

	if list.head.next.value != 2{
		t.FailNow()
	}

	if list.head.next.next.value != 1 {
		t.FailNow()
	}

	if list.head.next.next.next.value != 3 {
		t.FailNow()
	}

	if list.head.next.next.next.next.value != 5 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.value != 4 {
		t.FailNow()
	}

}

func TestRearrangeLinkedList3(t *testing.T) {
	list := getLinkedList()
	list.setHead(&node{3, nil, false})
	list.add(&node{0, nil, false})
	list.add(&node{5, nil, false})
	list.add(&node{2, nil, false})
	list.add(&node{1, nil, false})
	list.add(&node{4, nil, false})
	rearrangeLinkedList(list, 3)

	if list.head.value != 0 {
		t.FailNow()
	}

	if list.head.next.value != 2{
		t.FailNow()
	}

	if list.head.next.next.value != 1 {
		t.FailNow()
	}

	if list.head.next.next.next.value != 3 {
		t.FailNow()
	}

	if list.head.next.next.next.next.value != 5 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.value != 4 {
		t.FailNow()
	}

}

func TestRearrangeLinkedList4(t *testing.T) {
	list := getLinkedList()
	list.setHead(&node{0, nil, false})
	list.add(&node{3, nil, false})
	list.add(&node{2, nil, false})
	list.add(&node{1, nil, false})
	list.add(&node{5, nil, false})
	list.add(&node{4, nil, false})
	rearrangeLinkedList(list, 3)

	if list.head.value != 0 {
		t.FailNow()
	}

	if list.head.next.value != 2{
		t.FailNow()
	}

	if list.head.next.next.value != 1 {
		t.FailNow()
	}

	if list.head.next.next.next.value != 3 {
		t.FailNow()
	}

	if list.head.next.next.next.next.value != 5 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.value != 4 {
		t.FailNow()
	}

}

func TestRearrangeLinkedList5(t *testing.T) {
	list := getLinkedList()
	list.setHead(&node{0, nil, false})
	list.add(&node{3, nil, false})
	list.add(&node{2, nil, false})
	list.add(&node{1, nil, false})
	list.add(&node{4, nil, false})
	list.add(&node{5, nil, false})
	list.add(&node{3, nil, false})
	list.add(&node{-1, nil, false})
	list.add(&node{-2, nil, false})
	list.add(&node{3, nil, false})
	list.add(&node{6, nil, false})
	list.add(&node{7, nil, false})
	list.add(&node{3, nil, false})
	list.add(&node{2, nil, false})
	list.add(&node{-9000, nil, false})

	rearrangeLinkedList(list, 3)

	//list.print()

	if list.head.value != 0 {
		t.FailNow()
	}

	if list.head.next.value != 2{
		t.FailNow()
	}

	if list.head.next.next.value != 1 {
		t.FailNow()
	}

	if list.head.next.next.next.value != -1 {
		t.FailNow()
	}

	if list.head.next.next.next.next.value != -2 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.value != 2 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.value != -9000 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.value != 3 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.next.value != 3 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.next.next.value != 3 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.next.next.next.value != 3 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.next.next.next.next.value != 4 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.next.next.next.next.next.value != 5 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.next.next.next.next.next.next.value != 6 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.next.next.next.next.next.next.next.value != 7 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next != nil {
		t.FailNow()
	}



}

func TestRearrangeLinkedList6(t *testing.T) {
	list := getLinkedList()
	list.setHead(&node{0, nil, false})
	list.add(&node{3, nil, false})
	list.add(&node{2, nil, false})
	list.add(&node{1, nil, false})
	list.add(&node{3, nil, false})
	list.add(&node{5, nil, false})
	list.add(&node{3, nil, false})
	list.add(&node{3, nil, false})
	list.add(&node{-2, nil, false})
	list.add(&node{3, nil, false})
	list.add(&node{3, nil, false})
	list.add(&node{3, nil, false})
	list.add(&node{3, nil, false})
	list.add(&node{2, nil, false})
	list.add(&node{-9, nil, false})

	rearrangeLinkedList(list, 3)

	//list.print()

	if list.head.value != 0 {
		t.FailNow()
	}

	if list.head.next.value != 2{
		t.FailNow()
	}

	if list.head.next.next.value != 1 {
		t.FailNow()
	}

	if list.head.next.next.next.value != -2 {
		t.FailNow()
	}

	if list.head.next.next.next.next.value != 2 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.value != -9 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.value != 3 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.value != 3 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.next.value != 3 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.next.next.value != 3 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.next.next.next.value != 3 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.next.next.next.next.value != 3 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.next.next.next.next.next.value != 3 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.next.next.next.next.next.next.value != 3 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.next.next.next.next.next.next.next.value != 5 {
		t.FailNow()
	}

	if list.head.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next != nil {
		t.FailNow()
	}

}


