package linkedlist

import "fmt"

type linkedList struct {
	head *node
}

func getLinkedList() *linkedList {
	return &linkedList{}
}

func (l *linkedList) setHead(node *node) {
	if l.head == nil {
		l.head = node
	} else {
		temp := l.head
		l.head = node
		node.next = temp

	}
}

func (l *linkedList) add(node *node) {

	if l.head == nil {
		l.head = node
	}

	temp := l.head
	for temp.next != nil {
		temp = temp.next
	}
	temp.next = node
}

func (l *linkedList) print() {
	temp := l.head
	for temp != nil {
		fmt.Println(temp.value)
		temp = temp.next
	}
}

func (l *linkedList) findMiddle() int {

	first := l.head
	second := l.head

	for second != nil {
		second = second.next

		if second != nil {
			second = second.next
			first = first.next
		}

	}

	return first.value

}

func (l *linkedList) findkThElementFromLast(kthIdx int) int {
	first := l.head
	second := l.head

	for second != nil {
		second = second.next
		kthIdx--
		if kthIdx < 0 {
			first = first.next
		}

	}

	if kthIdx <= 0 {
		return first.value
	}

	return -1
}

func (l *linkedList) getMergedElement(m *linkedList) int {
	first := l.head
	second := m.head

	for first != nil && second != nil {
		if first == second || first.visited {
			return first.value
		}
		if second.visited {
			return second.value
		}
		first.visited = true
		second.visited = true
		first = first.next
		second = second.next

	}
	return -1
}

// list1 7 -> 1 -> 2
// list2 3 -> 4 -> 5
// result 0 -> 6 -> 7
func (l *linkedList) sumOfTwoLinkedListsForward(m *linkedList) *linkedList {

	if l == nil {
		return m
	}

	if m == nil {
		return l
	}
	first := l.head
	second := m.head
	result := getLinkedList()
	div := 0

	for first != nil && second != nil {
		val1 := first.value
		val2 := second.value

		sum := val1 + val2 + div

		div = sum / 10
		rem := sum % 10
		node1 := node{rem, nil, false}
		if result.head == nil {
			result.setHead(&node1)
		} else {
			result.add(&node1)
		}

		first = first.next
		second = second.next
	}

	for first != nil {
		sum := first.value + div
		div = sum / 10
		rem := sum % 10
		node1 := node{rem, nil, false}
		result.add(&node1)
		first = first.next
	}

	for second != nil {
		sum := second.value + div
		div = sum / 10
		rem := sum % 10
		node1 := node{rem, nil, false}
		result.add(&node1)
		second = second.next
	}

	if div != 0 {
		result.add(&node{div, nil, false})
	}

	return result
}

func (l *linkedList) reverseLinkedList() *linkedList {
	result := getLinkedList()
	temp := l.head
	for temp != nil {
		tempList := getLinkedList()
		t1 := temp.next
		temp.next = result.head
		tempList.setHead(temp)
		result = tempList
		temp = t1
	}
	return result
}

// // list1 7 -> 1 -> 2
// // list2 3 -> 4 -> 5
// // result 1 -> 0 -> 5 -> 7
// func (l *linkedList) sumOfTwoLinkedListsReverse(m *linkedList) *linkedList {

// }
