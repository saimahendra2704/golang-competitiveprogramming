package linkedlist

import (
	//"fmt"
	"testing"
)

func TestAddHead(t *testing.T) {
	list := getLinkedList()
	node1 := node{10, nil, false}
	node2 := node{20, nil, false}
	node3 := node{30, nil, false}
	node4 := node{40, nil, false}
	node5 := node{50, nil, false}
	node6 := node{60, nil, false}
	node7 := node{70, nil, false}

	list.setHead(&node1)
	list.add(&node2)
	list.add(&node3)
	list.add(&node4)
	list.add(&node5)
	list.add(&node6)
	list.add(&node7)
	list.print()
}

func TestFindMiddleOfLinkedList(t *testing.T) {
	list := getList()
	value := list.findMiddle()
	if value != 40 {
		t.FailNow()
	}
}

func TestFindMiddleOfLinkedList1(t *testing.T) {
	list := getList()
	temp := node{80, nil, false}
	list.add(&temp)
	value := list.findMiddle()
	if value != 50 {
		t.FailNow()
	}
}

func TestFindMiddleOfLinkedList2(t *testing.T) {
	list := getLinkedList()
	temp := node{80, nil, false}
	list.setHead(&temp)
	value := list.findMiddle()
	if value != 80 {
		t.FailNow()
	}
}

func getList() *linkedList {
	list := getLinkedList()
	node1 := node{10, nil, false}
	node2 := node{20, nil, false}
	node3 := node{30, nil, false}
	node4 := node{40, nil, false}
	node5 := node{50, nil, false}
	node6 := node{60, nil, false}
	node7 := node{70, nil, false}

	list.setHead(&node1)
	list.add(&node2)
	list.add(&node3)
	list.add(&node4)
	list.add(&node5)
	list.add(&node6)
	list.add(&node7)
	return list
}

func getAnotherList() (*linkedList, *linkedList) {
	list := getLinkedList()
	node1 := node{10, nil, false}
	node2 := node{20, nil, false}
	node3 := node{30, nil, false}
	node4 := node{40, nil, false}
	node5 := node{50, nil, false}
	node6 := node{60, nil, false}
	node7 := node{70, nil, false}

	list.setHead(&node1)
	list.add(&node2)
	list.add(&node3)
	list.add(&node4)
	list.add(&node5)
	list.add(&node6)
	list.add(&node7)

	anotheList := getLinkedList()
	anode1 := node{10, nil, false}
	anode2 := node{20, nil, false}
	anode3 := node{30, nil, false}
	anode4 := node{40, nil, false}
	//anode5 := node{50, nil,false}
	anode6 := node{60, nil, false}
	anode7 := node{70, nil, false}

	anotheList.setHead(&anode1)
	anotheList.add(&anode2)
	anotheList.add(&anode3)
	anotheList.add(&anode4)
	anotheList.add(&node5)
	anotheList.add(&anode6)
	anotheList.add(&anode7)

	return list, anotheList
}

func getAnotherListWithNoMeetingNodes() (*linkedList, *linkedList) {
	list := getLinkedList()
	node1 := node{10, nil, false}
	node2 := node{20, nil, false}
	node3 := node{30, nil, false}
	node4 := node{40, nil, false}
	node5 := node{50, nil, false}
	node6 := node{60, nil, false}
	node7 := node{70, nil, false}

	list.setHead(&node1)
	list.add(&node2)
	list.add(&node3)
	list.add(&node4)
	list.add(&node5)
	list.add(&node6)
	list.add(&node7)

	anotheList := getLinkedList()
	anode1 := node{10, nil, false}
	anode2 := node{20, nil, false}
	anode3 := node{30, nil, false}
	anode4 := node{40, nil, false}
	anode5 := node{50, nil, false}
	anode6 := node{60, nil, false}
	anode7 := node{70, nil, false}

	anotheList.setHead(&anode1)
	anotheList.add(&anode2)
	anotheList.add(&anode3)
	anotheList.add(&anode4)
	anotheList.add(&anode5)
	anotheList.add(&anode6)
	anotheList.add(&anode7)

	return list, anotheList
}

func TestGetKthLinkedList1(t *testing.T) {
	list := getList()
	value := list.findkThElementFromLast(1)
	if value != 70 {
		t.FailNow()
	}

	value = list.findkThElementFromLast(2)
	if value != 60 {
		t.FailNow()
	}

	value = list.findkThElementFromLast(20)
	if value != -1 {
		t.FailNow()
	}

	value = list.findkThElementFromLast(7)

	//fmt.Println(value)
	if value != 10 {
		t.FailNow()
	}
}

func TestFindMeetingPointOfTwoElements(t *testing.T) {
	l, m := getAnotherList()
	value := l.getMergedElement(m)
	if value != 50 {
		t.FailNow()
	}
}

func TestFindMeetingPointOfTwoElements1(t *testing.T) {
	l, m := getAnotherListWithNoMeetingNodes()
	value := l.getMergedElement(m)
	if value != -1 {
		t.FailNow()
	}
}

func TestSumOfTwoLinkedListsForwardWithEqualSizeLists(t *testing.T) {
	l := getLinkedList()
	lnode1 := node{7, nil, false}
	lnode2 := node{1, nil, false}
	lnode3 := node{2, nil, false}
	l.setHead(&lnode1)
	l.add(&lnode2)
	l.add(&lnode3)

	m := getLinkedList()
	mnode1 := node{3, nil, false}
	mnode2 := node{4, nil, false}
	mnode3 := node{5, nil, false}
	m.setHead(&mnode1)
	m.add(&mnode2)
	m.add(&mnode3)

	result := l.sumOfTwoLinkedListsForward(m)
	if result.head.value != 0 {
		t.FailNow()
	}
	if result.head.next.value != 6 {
		t.FailNow()
	}

	if result.head.next.next.value != 7 {
		t.FailNow()
	}
}

func TestSumOfTwoLinkedListsForwardWithEqualSizeLists1(t *testing.T) {
	l := getLinkedList()
	lnode1 := node{9, nil, false}
	lnode2 := node{9, nil, false}
	lnode3 := node{9, nil, false}
	l.setHead(&lnode1)
	l.add(&lnode2)
	l.add(&lnode3)

	m := getLinkedList()
	mnode1 := node{9, nil, false}
	mnode2 := node{9, nil, false}
	mnode3 := node{9, nil, false}
	m.setHead(&mnode1)
	m.add(&mnode2)
	m.add(&mnode3)

	result := l.sumOfTwoLinkedListsForward(m)
	if result.head.value != 8 {
		t.FailNow()
	}
	if result.head.next.value != 9 {
		t.FailNow()
	}

	if result.head.next.next.value != 9 {
		t.FailNow()
	}

	if result.head.next.next.next.value != 1 {
		t.FailNow()
	}
}

func TestSumOfTwoLinkedListsForwardWithEqualSizeLists2(t *testing.T) {
	l := getLinkedList()
	lnode1 := node{9, nil, false}
	lnode2 := node{9, nil, false}
	lnode3 := node{9, nil, false}
	l.setHead(&lnode1)
	l.add(&lnode2)
	l.add(&lnode3)

	m := getLinkedList()
	mnode1 := node{1, nil, false}
	m.setHead(&mnode1)

	result := l.sumOfTwoLinkedListsForward(m)
	if result.head.value != 0 {
		t.FailNow()
	}
	if result.head.next.value != 0 {
		t.FailNow()
	}

	if result.head.next.next.value != 0 {
		t.FailNow()
	}

	if result.head.next.next.next.value != 1 {
		t.FailNow()
	}
}

func TestSumOfTwoLinkedListsForwardWithEqualSizeLists3(t *testing.T) {
	l := getLinkedList()
	lnode1 := node{9, nil, false}
	lnode2 := node{9, nil, false}
	lnode3 := node{9, nil, false}
	l.setHead(&lnode1)
	l.add(&lnode2)
	l.add(&lnode3)

	//m := node{}

	result := l.sumOfTwoLinkedListsForward(nil)
	if result.head.value != 9 {
		t.FailNow()
	}
	if result.head.next.value != 9 {
		t.FailNow()
	}

	if result.head.next.next.value != 9 {
		t.FailNow()
	}

}


func TestReverseLinkedList1(t *testing.T){
	list := getList()
	tempList := list.reverseLinkedList()
	//tempList.print()
	if tempList.head.value != 70 {
		t.FailNow()
	} else if tempList.head.next.value != 60 {
		t.FailNow()
	} else if tempList.head.next.next.value != 50 {
		t.FailNow()
	} else if tempList.head.next.next.next.value != 40 {
		t.FailNow()
	} else if tempList.head.next.next.next.next.value != 30 {
		t.FailNow()
	} else if tempList.head.next.next.next.next.next.value != 20 {
		t.FailNow()
	} else if tempList.head.next.next.next.next.next.next.value != 10 {
		t.FailNow()
	}
}

func TestReverseLinkedList2(t *testing.T){
	list := getLinkedList()
	list.setHead(&node{10,nil,false})
	list.add(&node{20,nil,false})
	tempList := list.reverseLinkedList()
	//tempList.print()
	if tempList.head.value != 20 {
		t.FailNow()
	}else if tempList.head.next.value != 10 {
		t.FailNow()
	}
}
