package linkedlist

func rearrangeLinkedList(l *linkedList, value int) *linkedList {

	head := l.head

	var lessHead, lessTail *node
	var equalHead, equalTail *node
	var greaterHead, greaterTail *node

	for head != nil {

		tempHead := head.next
		head.next = nil
		if head.value < value {
			lessHead, lessTail = extractNode(lessHead, lessTail, head)
		} else if head.value > value {
			greaterHead, greaterTail = extractNode(greaterHead, greaterTail, head)
		} else {
			equalHead, equalTail = extractNode(equalHead, equalTail, head)
		}
		head = tempHead
	}
	if lessTail != nil {
		lessTail.next = equalHead
		if equalTail != nil {
			equalTail.next = greaterHead
		}
		l.head = lessHead
	} else if equalTail != nil {
		equalTail.next = greaterHead
		l.head = equalHead
	} else {
		l.head = greaterHead
	}

	return l
}

func extractNode(lessHead, lessTail, head *node) (*node, *node) {
	if lessHead == nil {
		lessHead = head
		lessTail = head
	} else {
		lessTail.next = head
		lessTail = head
	}
	return lessHead, lessTail
}
