package dynamicprogramming

import (
	"testing"
	"github.com/stretchr/testify/require"
)


func TestLongestIncreasingSubSequence1(t *testing.T){
	expected := []int{-24, 2, 3, 5, 6, 35}
	input := []int{5, 7, -24, 12, 10, 2, 3, 12, 5, 6, 35}
	output := LongestIncreasingSubsequence(input)
	require.Equal(t, expected, output)
}

func TestLongestIncreasingSubSequence2(t *testing.T){
	expected := []int{-1}
	input := []int{-1}
	output := LongestIncreasingSubsequence(input)
	require.Equal(t, expected, output)
}

func TestLongestIncreasingSubSequence3(t *testing.T){
	expected := []int{-1,2}
	input := []int{-1,2}
	output := LongestIncreasingSubsequence(input)
	require.Equal(t, expected, output)
}

func TestLongestIncreasingSubSequence4(t *testing.T){
	expected := []int{-1,1,2}
	input := []int{-1,2,1,2}
	output := LongestIncreasingSubsequence(input)
	require.Equal(t, expected, output)
}