package dynamicprogramming

func LongestIncreasingSubsequence(input []int) []int {
	// Write your code here.
	if len(input) == 1{
		return input
	}
	lengthArr := make([]int, len(input))
	seq := make([]int, len(input))
	maxSeqLength := -1
	var maxSeqLengthIdx int
	

	for i := 0; i < len(input); i++ {
		lengthArr[i] = 1
		seq[i] = -1
	}

	for i := 1; i < len(input); i++ {
		for j := i - 1; j >= 0; j-- {
			if input[i] > input[j] {
				temp := max(lengthArr[i], 1+lengthArr[j])
				if temp > lengthArr[i] {
					seq[i] = j
					lengthArr[i] = temp
				}

			}
		}
		maxSeqLength = max(lengthArr[i],maxSeqLength)

		if maxSeqLength == lengthArr[i]{
			maxSeqLengthIdx = i
		}
	}

	longestIncreasingSubSequence := make([]int,maxSeqLength)

	for k := maxSeqLength-1 ; k >=0 ; k--{
		val := input[maxSeqLengthIdx]
		longestIncreasingSubSequence[k] = val
		maxSeqLengthIdx = seq[maxSeqLengthIdx]
	}

	return longestIncreasingSubSequence
}

func max(val1, val2 int) int {
	if val1 > val2 {
		return val1
	} else if val2 > val1 {
		return val2
	} else {
		return val1
	}
}
