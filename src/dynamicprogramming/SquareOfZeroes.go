package dynamicprogramming

func SquareOfZeroes(matrix [][]int) bool {

	for i := 0; i < len(matrix)-1; i++ {
		for j := 0; j < len(matrix[i])-1; j++ {
			if matrix[i][j] == 0 {
				if matrix[i+1][j] == 0 && matrix[i][j+1] == 0 {
					if matrix[i+1][j+1] == 0 {
						return true
					}
					for k := 2; i+k < len(matrix); k++ {
						if checkForSquare(matrix,i,j,k){
							return true
						}
					}

				}
			}
		}
	}

	return false
}

func checkForSquare(matrix [][]int, i, j,k int) bool {
	ti, tj := i, j
	for j+1 <= k{
		if(matrix[i][j+1] == 1){
			return false;
		}
		j++
	}

	for i+1 <= ti+k {
		if(matrix[i+1][j] == 1){
			return false;
		}
		i++
	}
	for j-1 >= tj {
		if(matrix[i][j-1] == 1){
			return false;
		}
		j--
	}

	for i-1 >= ti {
		if(matrix[i-1][j] == 1){
			return false;
		}
		i--
	}
	if i == ti && j == tj {
		return true
	}

	return false
}

