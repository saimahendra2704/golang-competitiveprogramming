package dynamicprogramming

import (
	"sort"
	"strings"
)

type stringchain struct {
	stringValue string
	maxLength   int
	isProcessed bool
	next        *stringchain
}

func getChainObj(str string) *stringchain {
	return &stringchain{
		stringValue: str,
		maxLength:   1,
		isProcessed: false,
		next:        nil,
	}
}

func LongestStringChain(strings []string) []string {

	if len(strings) <= 1 {
		return strings
	}

	sort.Slice(strings, func(i, j int) bool { return len(strings[i]) > len(strings[j]) })
	smallestStringLength := len(strings[len(strings)-1])
	chainObjMap := make(map[string]*stringchain)
	rootEle := getChainObj("")
	maxLength := 0

	for _, str := range strings {
		val, ok := chainObjMap[str]
		if ok && val.isProcessed {
			continue
		} else {
			chainObj := getChainObj(str)
			chainObjMap[str] = chainObj
			expectedLength := len(str) - 1

			for i := 0; i < len(strings); i++ {
				if len(strings[i]) == expectedLength && oneCharDiff(str, strings[i]) {
					returnValue := calculateLongestStringChain(strings[i], chainObjMap, strings, smallestStringLength)
					if returnValue != nil {
						if returnValue.maxLength+1 > chainObj.maxLength {
							chainObj.maxLength = returnValue.maxLength + 1
							chainObj.next = returnValue
						}
					} 
				}
			}
			if chainObj.maxLength > maxLength{
				rootEle = chainObj
				maxLength = chainObj.maxLength
			}
		}

	}
	
	returnStrings := []string{}

	if rootEle.maxLength == 1{
		return returnStrings
	}

	for rootEle != nil  {
		returnStrings = append(returnStrings,rootEle.stringValue)
		rootEle = rootEle.next
	}

	return returnStrings

}

func calculateLongestStringChain(str string, chainObjMap map[string]*stringchain, strings []string, smallestStringLength int) *stringchain {
	if len(str) == smallestStringLength {
		chainObj := getChainObj(str)
		chainObjMap[str] = chainObj
		chainObjMap[str].isProcessed = true
		return chainObj
	}

	val, ok := chainObjMap[str]

	if ok && val.isProcessed {
		return chainObjMap[str]
	}

	chainObj := getChainObj(str)
	chainObjMap[str] = chainObj
	chainObjMap[str].isProcessed = true
	expectedLength := len(str) - 1
	for i := 0; i < len(strings); i++ {
		if len(strings[i]) == expectedLength && oneCharDiff(str, strings[i]) {
			returnValue := calculateLongestStringChain(strings[i], chainObjMap, strings, smallestStringLength)
			if returnValue != nil {
				if returnValue.maxLength+1 > chainObj.maxLength {
					chainObj.maxLength = returnValue.maxLength + 1
					chainObj.next = returnValue
				}
			}

		}
	}

	return chainObj
}

func oneCharDiff(str1, str2 string) bool {
	missingCount := 0
	for _, v := range str2 {
		if !strings.ContainsRune(str1, v) {
			missingCount++
		}
	}
	if missingCount > 0 {
		return false
	}
	return true
}
