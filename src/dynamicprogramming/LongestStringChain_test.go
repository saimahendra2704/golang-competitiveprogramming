package dynamicprogramming

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestCaseLongestStringChain1(t *testing.T) {
	input := []string{"abde", "abc", "abd", "abcde", "ade", "ae", "1abde", "abcdef"}
	expected := []string{"abcdef", "abcde", "abde", "ade", "ae"}
	output := LongestStringChain(input)
	require.Equal(t, expected, output)
}

func TestCaseLongestStringChain2(t *testing.T) {
	input := []string{"abcdefg", "abcdef", "abcde", "abcd", "abc", "ab", "a"}
	expected := []string{"abcdefg", "abcdef", "abcde", "abcd", "abc", "ab", "a"}
	output := LongestStringChain(input)
	require.Equal(t, expected, output)
}

func TestCaseLongestStringChain3(t *testing.T) {
	input := []string{"abcdefg", "1234", "abdefg", "abdfg", "123", "12", "bg","g","12345","12a345"}
	expected := []string{"12a345", "12345", "1234", "123", "12"}
	output := LongestStringChain(input)
	require.Equal(t, expected, output)
}

func TestCaseLongestStringChain4(t *testing.T) {
	input := []string{"abcdefg1", "1234c", "abdefg2", "abdfg", "123", "122", "bgg","g","1a2345","12a345"}
	expected := []string{}
	output := LongestStringChain(input)
	require.Equal(t, expected, output)
}
