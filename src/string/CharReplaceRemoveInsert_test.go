package string

import "testing"

func TestCheckForReplacement(t *testing.T) {
	s1, s2 := "pale", "bale"
	ok := charReplaceRemoveInsert(s1, s2)
	if !ok {
		t.FailNow()
	}
}

func TestCheckForInvalidReplacement(t *testing.T) {
	s1, s2 := "pale", "bole"
	ok := charReplaceRemoveInsert(s1, s2)
	if ok {
		t.FailNow()
	}
}

func TestCheckForEditing(t *testing.T) {
	s1, s2 := "Apple", "Aple"
	ok := charReplaceRemoveInsert(s1, s2)
	if !ok {
		t.FailNow()
	}
}

func TestCheckForEditingInvalid(t *testing.T) {
	s1, s2 := "Apple", "Able"
	ok := charReplaceRemoveInsert(s1, s2)
	if ok {
		t.FailNow()
	}
}

func TestCheckForEditing1(t *testing.T) {
	s1, s2 := "Aple", "Apple"
	ok := charReplaceRemoveInsert(s1, s2)
	if !ok {
		t.FailNow()
	}
}
