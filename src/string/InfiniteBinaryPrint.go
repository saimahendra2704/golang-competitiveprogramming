package string



type BinaryNode struct {
	binaryValue string
	next *BinaryNode
}

func InfiniteBinaryPrint() []string{

	tail := &BinaryNode{
		binaryValue : "1",
		next : nil,
	}
	head := &BinaryNode{
		binaryValue : "0",
		next : tail,
	}

	i:= 0

	result := []string{}

	for i < 200 {
		value := head.binaryValue
		result = append(result,value)
		head = head.next

		newTail := &BinaryNode{
			binaryValue : value+"1",
			next: nil,
		}
		node := &BinaryNode{
			binaryValue : value+"0",
			next: newTail ,
		}
		tail.next = node
		tail = newTail
		i++
	}

	return result

}