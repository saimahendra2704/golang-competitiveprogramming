package string

import (
	"testing"
)

func TestIsUniqueCharacterStringWithDuplicateChars(t *testing.T) {
	isUniqueCharacterString := isUniqueCharacterString("aab")
	if isUniqueCharacterString {
		t.FailNow()
	}
}

func TestIsUniqueCharacterStringWithoutDuplicateChars(t *testing.T) {
	isUniqueCharacterString := isUniqueCharacterString("agb")
	if !isUniqueCharacterString {
		t.FailNow()
	}
}

func TestIsUniqueCharacterStringWithStringLengthMoreThan128Chars(t *testing.T) {
	isUniqueCharacterString := isUniqueCharacterString("agbslkdfasdhoashdnasdasdfaowihowqif;ksa;lnas;dfhaodiweiorwoidhfhsadflasdf;l;aksdf;asdjhfo;iwjheroiahjds;kf;askdnlf;asoiweiojrkjnsd;lknasd;flk")
	if isUniqueCharacterString {
		t.FailNow()
	}
}
