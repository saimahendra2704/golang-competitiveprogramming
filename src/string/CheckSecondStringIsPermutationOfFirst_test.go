package string

import "testing"

func TestCheckSecondStringIsPermutationOfFirst(t *testing.T) {
	isPermutation := checkSecondStringIsPermutationOfFirst("aabac", "aaabc")
	if !isPermutation {
		t.FailNow()
	}
}

func TestCheckSecondStringIsPermutationOfFirstWithTwoDifferentStrings(t *testing.T) {
	isPermutation := checkSecondStringIsPermutationOfFirst("aabacd", "aaabc")
	if isPermutation {
		t.FailNow()
	}
}

func TestCheckSecondStringIsPermutationOfFirstWithSpace(t *testing.T) {
	isPermutation := checkSecondStringIsPermutationOfFirst("aabac ", "aaabc ")
	if !isPermutation {
		t.FailNow()
	}
}

func TestCheckSecondStringIsPermutationOfFirstWithSpaces(t *testing.T) {
	isPermutation := checkSecondStringIsPermutationOfFirst("aabac     ", "aaabc ")
	if isPermutation {
		t.FailNow()
	}
}
