package string

func isUniqueCharacterString(input string) (bool) {

	inputLength := len(input)
	if inputLength > 128{
		return false
	}

	charMap := make(map[byte]bool)
	
	for i:= 0 ; i < inputLength ; i++ {

		_,ok := charMap[input[i]]
		if ok == true {
			return false;
		}
		charMap[input[i]] = true

	}

	return true
}