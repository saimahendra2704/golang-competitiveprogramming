package string

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestCase1(t *testing.T) {
	bigString := "abcd$ef$axb$c$"
	smallString := "$$abf"
	expected := "f$axb$"
	require.Equal(t,
		SmallestSubstringContaining(bigString, smallString), expected,
	)
}

func TestCase2(t *testing.T) {
	bigString := "abcdef"
	smallString := "fa"
	expected := "abcdef"
	require.Equal(t,
		SmallestSubstringContaining(bigString, smallString), expected,
	)
}

func TestCase3(t *testing.T) {
	bigString := "abcdef"
	smallString := "d"
	expected := "d"
	require.Equal(t,
		SmallestSubstringContaining(bigString, smallString), expected,
	)
}

func TestCase4(t *testing.T) {
	bigString := "a$fuu+afff+affaffa+a$Affab+a+a+$a$"
	smallString := "a+$aaAaaaa$++"
	expected := "affa+a$Affab+a+a+$a"
	require.Equal(t,
		SmallestSubstringContaining(bigString, smallString), expected,
	)
}