package string

import "testing"

func TestInfiniteBinaryPrint(t *testing.T){
	result := InfiniteBinaryPrint()
	sixLen := 0 
	for _,val := range result {
		if len(val) == 6{
			sixLen++
		}
	}

	if sixLen != 64{
		t.FailNow()
	}


}