package string

func checkSecondStringIsPermutationOfFirst(first string, second string) bool{
	if(len(first) != len(second)){
		return false
	}

	charMap := make(map[rune]int)

	for _,c := range first{
		val,ok := charMap[c]
		if ok == true{
			charMap[c] = val+1
			continue
		}
		charMap[c] = 1
	}

	for _,s := range second{

		val,ok := charMap[s]
		if ok == true{
			if val == 1{
				delete(charMap,s)
			} else{
				charMap[s] = val-1
			}
		} else {
			return false;
		}
	}

	return len(charMap) == 0 

}