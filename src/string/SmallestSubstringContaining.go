package string

//problem definition :
//   You're given two non-empty strings: a big string and a small string. Write a
//   function that returns the smallest substring in the big string that contains
//   all of the small string's characters.

// Time complexity O(b+s) where b is length of big string and s is length of small string
// Space Complexity O(b+s)

//import "fmt"

func SmallestSubstringContaining(bigString, smallString string) string {
	// Write your code here.
	smallStringMap := make(map[rune]int)

	for _, r := range smallString {
		_, ok := smallStringMap[r]
		if ok {
			smallStringMap[r] = smallStringMap[r] + 1
		} else {
			smallStringMap[r] = 1
		}
	}

	return findSmallestSubString(smallStringMap, bigString)
}

func findSmallestSubString(smallStringMap map[rune]int, bigstring string) string {

	smallStringCharCount := len(smallStringMap)
	bigStringCharCount := 0
	bigStringMap := make(map[rune]int)
	left, right := 0, 0
	smallestSubstring := ""

	for right < len(bigstring) {

		for right < len(bigstring) && smallStringCharCount != bigStringCharCount {
			bigStringCharCount = moveRightPointer(smallStringMap, bigStringMap, bigstring, right, bigStringCharCount)
			right++
		}
		if bigStringCharCount == smallStringCharCount {
			temp := bigstring[left:right]
			if smallestSubstring == "" {
				smallestSubstring = temp
			} else if len(temp) < len(smallestSubstring) {
				smallestSubstring = temp
			}
		}

		for left < len(bigstring) && bigStringCharCount == smallStringCharCount {
			br := rune(bigstring[left])
			_, ok := bigStringMap[br]
			if ok {
				bigStringMap[br] = bigStringMap[br] - 1
			}
			if bigStringMap[br] < smallStringMap[br] {
				bigStringCharCount--
			}
			left++
			if bigStringCharCount == smallStringCharCount {
				temp := bigstring[left:right]
				if smallestSubstring == "" {
					smallestSubstring = temp
				} else if len(temp) < len(smallestSubstring) {
					smallestSubstring = temp
				}
			}
		}
	}

	return smallestSubstring

}

func moveRightPointer(smallStringMap, bigStringMap map[rune]int, bigstring string, right, charCount int) int {
	br := rune(bigstring[right])
	_, ok := smallStringMap[br]
	if !ok {
		return charCount
	}

	_, ok = bigStringMap[br]

	if ok {
		bigStringMap[br] = bigStringMap[br] + 1
	} else {
		bigStringMap[br] = 1
	}

	if bigStringMap[br] == smallStringMap[br] {
		charCount++
	}
	return charCount
}
