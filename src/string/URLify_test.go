package string

import "testing"

func TestURLifyWithOutSpaces(t *testing.T) {
	input := "abc"
	urlify(input)
}

func TestURLifyWithSpaces(t *testing.T) {
	input := "ab c"
	urlify(input)
}
