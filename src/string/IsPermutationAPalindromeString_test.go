package string

import "testing"

func TestIsStringPermuationIsPalindrome(t *testing.T){
	input := "abab"
	isStringPermutationIsPalindrome := isStringPermutationIsPalindrome(input)

	if(!isStringPermutationIsPalindrome){
		t.FailNow()
	}

}

func TestIsStringPermuationIsPalindrome1(t *testing.T){
	input := "ababc"
	isStringPermutationIsPalindrome := isStringPermutationIsPalindrome(input)

	if(!isStringPermutationIsPalindrome){
		t.FailNow()
	}

}

func TestIsStringPermuationIsNotPalindrome(t *testing.T){
	input := "ababcd"
	isStringPermutationIsPalindrome := isStringPermutationIsPalindrome(input)

	if(isStringPermutationIsPalindrome){
		t.FailNow()
	}

}

func TestIsStringPermuationIsPalindromeWithSpaces(t *testing.T){
	input := "ababc   "
	isStringPermutationIsPalindrome := isStringPermutationIsPalindrome(input)

	if(!isStringPermutationIsPalindrome){
		t.FailNow()
	}

}

func TestIsStringPermuationIsNotPalindromeWithSpaces(t *testing.T){
	input := "ababcd   "
	isStringPermutationIsPalindrome := isStringPermutationIsPalindrome(input)

	if(isStringPermutationIsPalindrome){
		t.FailNow()
	}

}