package string



func charReplaceRemoveInsert(s1,s2 string) bool{
	if len(s1) == len(s2) {
		return checkForReplacement(s1,s2)
	}else if(len(s1) - len(s2) == 1 ){
		return checkForEditing(s1,s2)
	}else if(len(s1) - len(s2) == -1 ){
		return checkForEditing(s2,s1)
	}
	return false
}

func checkForReplacement(s1,s2 string) bool {
	noOfDifferences := 0
	idx1 := 0
	idx2 := 0

	for idx1 < len(s1) && idx2 < len(s2) {
		if s1[idx1] != s2[idx2] {
			noOfDifferences++
		}
		if (noOfDifferences > 1){
			return	false
		}
		idx1++
		idx2++
	}

	return true
}

func checkForEditing(s1,s2 string) bool{
	idx1 := 0
	idx2 := 0

	for idx1 < len(s1) && idx2 <len(s2){
		if s1[idx1] == s2[idx2]{
			idx1++
			idx2++
		}else{
			idx1++
		}
	}

	return idx1-idx2 == 1
}