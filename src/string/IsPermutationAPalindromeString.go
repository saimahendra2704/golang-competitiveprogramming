package string

func isStringPermutationIsPalindrome(input string) bool {
	charMap := make(map[rune]int)
	for _, i := range input {
		val, ok := charMap[i]
		if ok == true {
			charMap[i] = val + 1
		} else {
			charMap[i] = 1
		}
	}
	numberOfOddChars := 0
	for k,v := range charMap {

		if( k == ' '){
			continue
		}

		if v%2 == 1 {
			numberOfOddChars++
		}

	}
	return numberOfOddChars <= 1
}
