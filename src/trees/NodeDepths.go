package trees

//import "fmt"

type BinaryTree struct {
	Value       int
	Left, Right *BinaryTree
}

var sumOfNodeDepths int

func NodeDepths(root *BinaryTree) int {

	//NodeDepthsHelper(root,0)	
	return NodeDepthsHelper(root,0)
}

func NodeDepthsHelper(root *BinaryTree,  depth int) int {
	if root == nil {
		return 0
	}
	return depth+NodeDepthsHelper(root.Left, depth+1)+NodeDepthsHelper(root.Right,depth+1)
}

