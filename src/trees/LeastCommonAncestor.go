package trees

var found bool 
var element *TreeNode

func leastCommonAncestor(node1, node2, root *TreeNode) *TreeNode {

	if(root == node1 || root == node2 || root == nil){
		return root
	}

	left := leastCommonAncestor(node1,node2,root.left)
	right := leastCommonAncestor(node1,node2,root.right)

	if left != nil && right != nil && !found {
		found = true
		element =  root
	}else if left != nil && !found{
		return left
	}else if right != nil && !found{
		return right
	}
	return element
}
