package trees

type AncestralTree struct {
	Name     string
	Ancestor *AncestralTree
}

func GetYoungestCommonAncestor(top, descendantOne, descendantTwo *AncestralTree) *AncestralTree {
	// Write your code here.
	depthOne := getDepth(top, descendantOne)
	depthTwo := getDepth(top, descendantTwo)
	if depthOne > depthTwo {
		for depthOne != depthTwo {
			descendantOne = descendantOne.Ancestor
			depthOne--
		}
	} else if depthTwo > depthOne {
		for depthOne != depthTwo {
			descendantTwo = descendantTwo.Ancestor
			depthTwo--
		}
	}

	for descendantOne != descendantTwo {
		descendantOne = descendantOne.Ancestor
		descendantTwo = descendantTwo.Ancestor
	}

	return descendantOne
}

func getDepth(top, descendant *AncestralTree) int {
	depth := 0
	for descendant != top {
		descendant = descendant.Ancestor
		depth++
	}
	return depth
}
