package trees

import (
	"testing"
	//"fmt"
)

func TestInOrderTraversal(t *testing.T){

	node2 := &TreeNode{2,nil,nil}
	node1 := &TreeNode{1,nil,node2}
	node4 := &TreeNode{4,node1,nil} 
	node3 := &TreeNode{3,nil,nil}
	node7 := &TreeNode{7,node3,node4}

	node8 := &TreeNode{8,nil,nil}
	node3_1 := &TreeNode{3,node8,nil}
	node11 := &TreeNode{11,nil,nil}
	node9 := &TreeNode{9,node11,node3_1}

	root := &TreeNode{10,node7,node9}

	outputArr := TraverseInOrder(root)

	if outputArr[0] != 3 {
		t.FailNow()
	}	

	if outputArr[1] != 7 {
		t.FailNow()
	}

	if outputArr[2] != 1 {
		t.FailNow()
	}

	if outputArr[3] != 2 {
		t.FailNow()
	}

	if outputArr[4] != 4 {
		t.FailNow()
	}

	if outputArr[5] != 10 {
		t.FailNow()
	}

	if outputArr[6] != 11 {
		t.FailNow()
	}

	if outputArr[7] != 9 {
		t.FailNow()
	}

	if outputArr[8] != 8 {
		t.FailNow()
	}

	if outputArr[9] != 3 {
		t.FailNow()
	}
}