package trees

func postOrderTraversal(root *TreeNode) []int {
	if root == nil {
		return nil
	}
	
	postOrderTraversal(root.left)
	postOrderTraversal(root.right)
	output = append(output, root.value)

	return output
}
