package trees

import (
	"testing"
	//"fmt"
)

func TestLeastCommonAncestor1(t *testing.T){

	node2 := &TreeNode{2,nil,nil}
	node1 := &TreeNode{1,nil,node2}
	node4 := &TreeNode{4,node1,nil} 
	node3 := &TreeNode{3,nil,nil}
	node7 := &TreeNode{7,node3,node4}

	node8 := &TreeNode{8,nil,nil}
	node16 := &TreeNode{16,node8,nil}
	node11 := &TreeNode{11,nil,nil}
	node9 := &TreeNode{9,node11,node16}

	root := &TreeNode{10,node7,node9}

	output := leastCommonAncestor(node3,node4,root)

	if output != node7{
		t.FailNow()
	}

}

func TestLeastCommonAncestor2(t *testing.T){

	node2 := &TreeNode{2,nil,nil}
	node1 := &TreeNode{1,nil,node2}
	node4 := &TreeNode{4,node1,nil} 
	node3 := &TreeNode{3,nil,nil}
	node7 := &TreeNode{7,node3,node4}

	node8 := &TreeNode{8,nil,nil}
	node16 := &TreeNode{16,node8,nil}
	node11 := &TreeNode{11,nil,nil}
	node9 := &TreeNode{9,node11,node16}

	root := &TreeNode{10,node7,node9}

	output := leastCommonAncestor(node3,node9,root)

	if output != root{
		t.FailNow()
	}

}

func TestLeastCommonAncestor3(t *testing.T){

	node2 := &TreeNode{2,nil,nil}
	node1 := &TreeNode{1,nil,node2}
	node4 := &TreeNode{4,node1,nil} 
	node3 := &TreeNode{3,nil,nil}
	node7 := &TreeNode{7,node3,node4}

	node8 := &TreeNode{8,nil,nil}
	node16 := &TreeNode{16,node8,nil}
	node11 := &TreeNode{11,nil,nil}
	node9 := &TreeNode{9,node11,node16}

	root := &TreeNode{10,node7,node9}

	output := leastCommonAncestor(node7,node9,root)

	if output != root{
		t.FailNow()
	}

}

func TestLeastCommonAncestor4(t *testing.T){

	node2 := &TreeNode{2,nil,nil}
	node1 := &TreeNode{1,nil,node2}
	node4 := &TreeNode{4,node1,nil} 
	node3 := &TreeNode{3,nil,nil}
	node7 := &TreeNode{7,node3,node4}

	node8 := &TreeNode{8,nil,nil}
	node16 := &TreeNode{16,node8,nil}
	node11 := &TreeNode{11,nil,nil}
	node9 := &TreeNode{9,node11,node16}

	root := &TreeNode{10,node7,node9}

	output := leastCommonAncestor(node4,node16,root)

	if output != root{
		t.FailNow()
	}

}

func TestLeastCommonAncestor5(t *testing.T){

	node2 := &TreeNode{2,nil,nil}
	node1 := &TreeNode{1,nil,node2}
	node4 := &TreeNode{4,node1,nil} 
	node3 := &TreeNode{3,nil,nil}
	node7 := &TreeNode{7,node3,node4}

	node8 := &TreeNode{8,nil,nil}
	node16 := &TreeNode{16,node8,nil}
	node11 := &TreeNode{11,nil,nil}
	node9 := &TreeNode{9,node11,node16}

	root := &TreeNode{10,node7,node9}

	output := leastCommonAncestor(node3,node7,root)

	if output != node7{
		t.FailNow()
	}

}