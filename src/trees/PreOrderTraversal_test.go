package trees

import (
	"testing"
	//"fmt"
)

func TestPreOrderTraversal(t *testing.T){

	node2 := &TreeNode{2,nil,nil}
	node1 := &TreeNode{1,nil,node2}
	node4 := &TreeNode{4,node1,nil} 
	node3 := &TreeNode{3,nil,nil}
	node7 := &TreeNode{7,node3,node4}

	node8 := &TreeNode{8,nil,nil}
	node3_1 := &TreeNode{3,node8,nil}
	node11 := &TreeNode{11,nil,nil}
	node9 := &TreeNode{9,node11,node3_1}

	root := &TreeNode{10,node7,node9}

	outputArr := preOrderTraversal(root)

	if outputArr[0] != 10 {
		t.FailNow()
	}	

	if outputArr[1] != 7 {
		t.FailNow()
	}

	if outputArr[2] != 3 {
		t.FailNow()
	}

	if outputArr[3] != 4 {
		t.FailNow()
	}

	if outputArr[4] != 1 {
		t.FailNow()
	}

	if outputArr[5] != 2 {
		t.FailNow()
	}

	if outputArr[6] != 9 {
		t.FailNow()
	}

	if outputArr[7] != 11 {
		t.FailNow()
	}

	if outputArr[8] != 3 {
		t.FailNow()
	}

	if outputArr[9] != 8 {
		t.FailNow()
	}
}