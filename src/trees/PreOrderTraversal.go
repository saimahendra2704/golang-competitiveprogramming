package trees

func preOrderTraversal(root *TreeNode) []int {
	if root == nil {
		return nil
	}
	output = append(output, root.value)
	preOrderTraversal(root.left)
	preOrderTraversal(root.right)

	return output
}
