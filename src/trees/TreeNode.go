package trees

type TreeNode struct {
	value int
	left  *TreeNode
	right *TreeNode
}
