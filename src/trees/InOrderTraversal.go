package trees

var output []int
var emptyRes []int

func TraverseInOrder(root *TreeNode) []int {
	if root == nil {
		return nil
	}
	TraverseInOrder(root.left)
	output = append(output, root.value)
	TraverseInOrder(root.right)
	return output
}
