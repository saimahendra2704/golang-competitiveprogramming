package stack

import "fmt"

type node struct {
	value int
	next  *node
}

type stack struct {
	top *node
}

func getStack() *stack {
	return &stack{}
}

func (s *stack) add(elem *node) {
	//top := s.top
	if s.top == nil {
		s.top = elem
	} else {
		temp := s.top
		s.top = elem
		s.top.next = temp
	}
}

func (s *stack) print() {
	top := s.top
	for top != nil {
		fmt.Println(top.value)
		top = top.next
	}
}
