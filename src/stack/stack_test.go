package stack

import "testing"

func TestCreateStackAndAddElements(t *testing.T) {
	stack := getStack()
	stack.add(&node{10, nil})
	stack.add(&node{20, nil})
	stack.add(&node{30, nil})
	stack.add(&node{40, nil})
	stack.add(&node{50, nil})
	stack.add(&node{60, nil})
	if stack.top.value != 60 {
		t.FailNow()
	}
	if stack.top.next.value != 50 {
		t.FailNow()
	}
	if stack.top.next.next.value != 40 {
		t.FailNow()
	}
	if stack.top.next.next.next.value != 30 {
		t.FailNow()
	}
	if stack.top.next.next.next.next.value != 20 {
		t.FailNow()
	}
	if stack.top.next.next.next.next.next.value != 10 {
		t.FailNow()
	}
}
