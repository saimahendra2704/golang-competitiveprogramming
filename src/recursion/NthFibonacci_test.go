package recursion

import "testing"

func TestNthFibonacci(t *testing.T){
	if GetNthFib(6) != 5{
		t.FailNow()
	}
}

func TestNthFibonacci1(t *testing.T){
	if GetNthFib(1) != 0{
		t.FailNow()
	}
}

func TestNthFibonacci2(t *testing.T){
	if GetNthFib(2) != 1{
		t.FailNow()
	}
}