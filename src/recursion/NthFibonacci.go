package recursion


func GetNthFib(n int) int {

	if n == 2 || n == 1 {
		return n-1
	}
	return GetNthFib(n-1)+GetNthFib(n-2)
}