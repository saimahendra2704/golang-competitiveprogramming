package arrays

import "testing"

func TestContiguousSubArraySumTest1(t *testing.T){
	input := []int{1,2,3,7,5}
	target := 12
	result := ContiguousSubArraySum(input,target)
	if result[0] != 1 && result[1] != 3{
		t.FailNow()
	}
}

func TestContiguousSubArraySumTest2(t *testing.T){
	input := []int{1,2,8,9,2,1,1,3,7,5}
	target := 12
	result := ContiguousSubArraySum(input,target)
	if result[0] != 3 && result[1] != 5{
		t.FailNow()
	}
}

func TestContiguousSubArraySumTest3(t *testing.T){
	input := []int{1,2,8,9,2,1,1,3,7,5}
	target := 21
	result := ContiguousSubArraySum(input,target)
	if result[0] != 0 && result[1] != 4{
		t.FailNow()
	}
}