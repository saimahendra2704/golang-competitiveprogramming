package arrays

import (
	"testing"
	"fmt"
)

func TestTwoNumberSum(t *testing.T){
	input := []int {1,3,5,6,7,8,9}
	result := twoNumberSum(input,10)
	if len(result) == 0{
		t.FailNow()
	}
	for _,v := range result{
		fmt.Printf("Index of elements whose sum is equal to target %d",v)
		fmt.Println()
	}
}

func TestTwoNumberSum1(t *testing.T){
	input := []int {3,5,-4,8,11,1,-1,6}
	result := twoNumberSum(input,10)
	if len(result) == 0{
		t.FailNow()
	}
	for _,v := range result{
		fmt.Printf("Index of elements whose sum is equal to target %d",v)
		fmt.Println()
	}
}

func TestTwoNumberSum2(t *testing.T){
	input := []int {4,6}
	result := twoNumberSum(input,10)
	if len(result) == 0{
		t.FailNow()
	}
	for _,v := range result{
		fmt.Printf("Index of elements whose sum is equal to target %d",v)
		fmt.Println()
	}
}

func TestTwoNumberSum3(t *testing.T){
	input := []int {4,6}
	result := twoNumberSum(input,11)
	if len(result) != 0{
		t.FailNow()
	}
	for _,v := range result{
		fmt.Printf("Index of elements whose sum is equal to target %d",v)
		fmt.Println()
	}
}