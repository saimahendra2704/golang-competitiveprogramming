package arrays

func ContiguousSubArraySum(input []int,target int) []int{
	i ,j := 0,0
	sum := 0 
	for j < len(input){
		sum += input[j]
		if sum == target {
			result := []int{i,j}
			return result
		}else if sum > target{
			for i < len (input)  {
				sum -= input[i]
				i++
				if sum == target {
					result := []int{i,j}
					return result
				}else if sum < target{
					 break
				}
				
			}
		}
		j++
	}

	return nil
}