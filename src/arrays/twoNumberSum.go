package arrays


func twoNumberSum(input []int, target int) ([][2]int){
	resultMap := make(map[int]int)
	result := [][2]int{}
	for i := 0 ; i < len(input) ; i++{
		inputValue := input[i]
		val,ok := resultMap[inputValue]
		if ok == true{
			tempResult := [2]int{}
			tempResult[0] = val
			tempResult[1] = i
			result = append(result,tempResult)
		}
		temp := target - inputValue;
		resultMap[temp] = i
	}
	return result
}
