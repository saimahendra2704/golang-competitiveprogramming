# Golang-CompetitiveProgramming

Competitive programming with Golang

#Two number sum problem
Given an array of integers and a target, return the indexes of numbers whose sum matches to target.

    